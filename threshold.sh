#!/bin/bash
#THRESHOLD VALUE IS 25

usage=`df -h . | awk -F " " 'NR>1{print$(NF-1)}' | sed 's/%//g'`

#above command will give only value from used column in df -h . by removing % symbol

if [ $usage -ge 20 ]
then
	echo "Threshold value reached" 
	echo "THRESHOLD VALUE REACHED" | mail -s "CRITICAL DISK USAGE" "yatishsai23@gmail.com"
fi

